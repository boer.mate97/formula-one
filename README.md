Test assignment for a CRUD API.

Web application framework: ASP.NET Core 
Database: SQLite (in-memory) 
Authentication: ASP.NET Core Identity
ORM: Entity Framework Core