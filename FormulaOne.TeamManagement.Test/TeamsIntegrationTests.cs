﻿using FormulaOne.TeamManagement.Model;
using FormulaOne.TeamManagement.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace FormulaOne.TeamManagement.Test
{
    public class TeamsIntegrationTests : IClassFixture<WebApplicationFactory<Startup>>, IDisposable
    {
        private readonly HttpClient _client;
        private readonly WebApplicationFactory<Startup> _webAppFactory;

        private readonly IEnumerable<Team> _originalData;
        private readonly IEnumerable<Team> _seededTestData;

        public TeamsIntegrationTests(WebApplicationFactory<Startup> webAppfactory)
        {
            _webAppFactory = webAppfactory ?? throw new ArgumentNullException(nameof(webAppfactory));
            _client = _webAppFactory.CreateClient();
            _client.BaseAddress = new Uri("https://localhost:44368/api/");
            _client.Timeout = TimeSpan.FromSeconds(5);

            using (var scope = _webAppFactory.Services.CreateScope())
            {
                var dbContext = (IFormulaOneDbContext)scope.ServiceProvider.GetRequiredService(typeof(IFormulaOneDbContext));
                _originalData = RetrieveStoredData(dbContext);
                _seededTestData = SeedTestData(dbContext);
            }

            // Authenticate session
            _client.PostAsync($"auth/authenticateSession?{AuthIntegrationTests.CorrectAuthParams}", null).Wait();
        }

        public void Dispose()
        {
            using var scope = _webAppFactory.Services.CreateScope();
            var dbContext = (IFormulaOneDbContext)scope.ServiceProvider.GetRequiredService(typeof(IFormulaOneDbContext));
            ClearTestData(dbContext, _originalData);
            _client.Dispose();
        }

        [Fact]
        public async Task GetTeams_ShouldReturnAllTeams()
        {
            // Assign
            var expectedData = _originalData.Concat(_seededTestData).ToList();

            // Act
            var response = await _client.GetAsync($"teams/getAll");
            var actualData = JsonConvert.DeserializeObject<IEnumerable<Team>>(await response.Content.ReadAsStringAsync()).ToList();

            // Assert
            Assert.Equal(StatusCodes.Status200OK, (int)response.StatusCode);
            Assert.Equal(expectedData.Count, actualData.Count);
            for (int i = 0; i < expectedData.Count; i++)
            {
                Assert.Equal(expectedData[i], actualData[i]);
            }
        }

        [Fact]
        public async Task GetTeamById_ShouldReturnGivenTeam()
        {
            // Assign
            var expectedRecord = _seededTestData.First();

            // Act
            var response = await _client.GetAsync($"teams/getById?id={expectedRecord.Id}");
            var actualData = JsonConvert.DeserializeObject<Team>(await response.Content.ReadAsStringAsync());

            // Assert
            Assert.Equal(StatusCodes.Status200OK, (int)response.StatusCode);
            Assert.Equal(expectedRecord, actualData);
        }

        [Fact]
        public async Task AddTeam_ShouldCreateGivenTeam()
        {
            // Assign
            var expectedRecord = new Team()
            {
                Id = Guid.NewGuid().ToString(),
                Name = "Teams/Add test team",
                ChampionshipWinCount = 3,
                EntryFeePaid = true,
                FoundedAt = new DateTime(2019, 3, 1)
            };
            var expectedLocationHeader = $"api/Teams/Add";
            var json = JsonConvert.SerializeObject(expectedRecord);

            // Act
            var response = await _client.PostAsync($"teams/add", new StringContent(json, Encoding.Default, "application/json"));
            var actualRecord = JsonConvert.DeserializeObject<Team>(await response.Content.ReadAsStringAsync());
            var actualLocationHeader = response.Headers.Location.ToString();

            // Assert
            Assert.Equal(StatusCodes.Status201Created, (int)response.StatusCode);
            Assert.Equal(expectedLocationHeader, actualLocationHeader);
            Assert.Equal(expectedRecord, actualRecord);

            using var scope = _webAppFactory.Services.CreateScope();
            var dbContext = scope.ServiceProvider.GetRequiredService<IFormulaOneDbContext>();
            var storedRecord = RetrieveRecord(dbContext, expectedRecord.Id);
            Assert.Equal(expectedRecord, storedRecord);
        }

        [Fact]
        public async Task UpdateTeam_ShouldUpdateGivenTeam()
        {
            // Assign
            var id = _seededTestData.First().Id;
            var expectedRecord = new Team()
            {
                Id = id,
                Name = "Teams/Update test team",
                ChampionshipWinCount = 3,
                EntryFeePaid = true,
                FoundedAt = new DateTime(2019, 3, 1)
            };
            var json = JsonConvert.SerializeObject(expectedRecord);

            // Act
            var response = await _client.PutAsync($"teams/update", new StringContent(json, Encoding.Default, "application/json"));

            // Assert
            Assert.Equal(StatusCodes.Status200OK, (int)response.StatusCode);

            using var scope = _webAppFactory.Services.CreateScope();
            var dbContext = scope.ServiceProvider.GetRequiredService<IFormulaOneDbContext>();
            var storedRecord = RetrieveRecord(dbContext, expectedRecord.Id);
            Assert.Equal(expectedRecord, storedRecord);
        }

        [Fact]
        public async Task RemoveTeam_ShouldRemoveGivenTeam()
        {
            // Assign
            var id = _seededTestData.First().Id;

            // Act
            var response = await _client.DeleteAsync($"teams/remove?id={id}");

            // Assert
            Assert.Equal(StatusCodes.Status204NoContent, (int)response.StatusCode);

            using var scope = _webAppFactory.Services.CreateScope();
            var dbContext = scope.ServiceProvider.GetRequiredService<IFormulaOneDbContext>();
            var storedRecord = RetrieveRecord(dbContext, id);
            Assert.Null(storedRecord);
        }

        private static Team RetrieveRecord(IFormulaOneDbContext context, string id)
        {
            return context.Teams.SingleOrDefault(t => t.Id == id);
        }

        private static IEnumerable<Team> RetrieveStoredData(IFormulaOneDbContext context)
        {
            return context.Teams.ToList();
        }

        private static IEnumerable<Team> SeedTestData(IFormulaOneDbContext context)
        {
            var seed = new List<Team>();
            for (int i = 0; i < 3; i++)
            {
                seed.Add(new Team()
                {
                    Id = Guid.NewGuid().ToString("D"),
                    Name = $"Initial team {i}",
                    ChampionshipWinCount = i,
                    EntryFeePaid = i % 2 == 0,
                    FoundedAt = new DateTime(2000, 1, 1).AddYears(i)
                });
            }
            context.Teams.AddRange(seed);
            context.SaveChanges();
            return seed;
        }

        private static void ClearTestData(IFormulaOneDbContext context, IEnumerable<Team> originalData)
        {
            var origIds = originalData.Select(d => d.Id);
            var testData = context.Teams.Where(t => !origIds.Contains(t.Id));
            context.Teams.RemoveRange(testData);
            context.SaveChanges();
        }
    }
}
