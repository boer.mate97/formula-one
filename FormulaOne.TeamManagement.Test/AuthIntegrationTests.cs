using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using System;
using System.Net;
using System.Net.Http;
using System.Threading;
using Xunit;

namespace FormulaOne.TeamManagement.Test
{
    public class AuthIntegrationTests : IClassFixture<WebApplicationFactory<Startup>>, IDisposable
    {
        private readonly HttpClient _client;
        private readonly WebApplicationFactory<Startup> _webAppFactory;

        public const string CorrectAuthParams = "userName=admin&password=f1test2018";
        private const string IncorrectAuthParams = "userName=admin&password=f1test2019";

        public AuthIntegrationTests(WebApplicationFactory<Startup> webAppfactory)
        {
            _webAppFactory = webAppfactory ?? throw new ArgumentNullException(nameof(webAppfactory));
            _client = _webAppFactory.CreateClient();
            _client.BaseAddress = new Uri("https://localhost:44368/api/");
            _client.Timeout = TimeSpan.FromSeconds(5);
        }

        public void Dispose()
        {
            _client.Dispose();
        }

        [Fact]
        public async void AuthenticateSession_ShouldReturnOk_OnCorrectCredentials()
        {
            // Act
            var response = await _client.PostAsync($"auth/authenticateSession?{CorrectAuthParams}", null);

            // Assert
            Assert.Equal(StatusCodes.Status200OK, (int)response.StatusCode);
        }

        [Fact]
        public async void AuthenticateSession_ShouldReturnUnauthorized_OnIncorrectCredentials()
        {
            // Act
            var response = await _client.PostAsync($"auth/authenticateSession?{IncorrectAuthParams}", null);

            // Assert
            Assert.Equal(StatusCodes.Status401Unauthorized, (int)response.StatusCode);
        }

        [Fact]
        public async void Ping_ShouldReturnOk_OnAuthenticatedSession()
        {
            // Assign
            await _client.PostAsync("auth/authenticateSession?userName=admin&password=f1test2018", null);

            // Act
            var response = await _client.GetAsync("auth/ping");

            // Assert
            Assert.Equal(StatusCodes.Status200OK, (int)response.StatusCode);
        }

        [Fact]
        public async void Ping_ShouldReturnUnauthorized_OnUnauthenticatedSession()
        {
            // Act
            var response = await _client.GetAsync("auth/ping");

            // Assert
            Assert.Equal(StatusCodes.Status401Unauthorized, (int)response.StatusCode);
        }
    }
}
