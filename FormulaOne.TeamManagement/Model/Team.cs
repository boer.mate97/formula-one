﻿using FormulaOne.TeamManagement.Attributes;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace FormulaOne.TeamManagement.Model
{
    [DataContract]
    public class Team : IEquatable<Team>
    {
        [DataMember(IsRequired = true)]
        [Key]
        [Required]
        [StringLength(36)]
        public string Id { get; set; }

        [DataMember(IsRequired = true)]
        [Required]
        [StringLength(30)]
        public string Name { get; set; }

        [DataMember(IsRequired = true)]
        [Required]
        [DatePassed]
        public DateTime? FoundedAt { get; set; }

        [DataMember]
        [Range(0, int.MaxValue)]
        public int? ChampionshipWinCount { get; set; }

        [DataMember]
        public bool? EntryFeePaid { get; set; }

        public override bool Equals(object obj)
        {
            return Equals(obj as Team);
        }

        public bool Equals(Team other)
        {
            return other != null &&
                   Id == other.Id &&
                   Name == other.Name &&
                   FoundedAt == other.FoundedAt &&
                   ChampionshipWinCount == other.ChampionshipWinCount &&
                   EntryFeePaid == other.EntryFeePaid;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Id, Name, FoundedAt, ChampionshipWinCount, EntryFeePaid);
        }

        public static bool operator ==(Team left, Team right)
        {
            return EqualityComparer<Team>.Default.Equals(left, right);
        }

        public static bool operator !=(Team left, Team right)
        {
            return !(left == right);
        }
    }
}
