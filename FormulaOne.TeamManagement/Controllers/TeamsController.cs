﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FormulaOne.TeamManagement.Services;
using FormulaOne.TeamManagement.Model;
using FormulaOne.TeamManagement.Attributes;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Authorization;

namespace FormulaOne.TeamManagement.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TeamsController : ControllerBase
    {
        private readonly IFormulaOneDbContext _dbContext;

        public TeamsController(IFormulaOneDbContext dbContext)
        {
            _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
        }

        [HttpGet(nameof(GetAll))]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> GetAll()
        {
            try
            {
                var teams = await _dbContext.Teams.ToListAsync();
                return Ok(teams);
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpGet(nameof(GetById))]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetById([FromQuery] string id)
        {
            if (string.IsNullOrEmpty(id)) return BadRequest();

            try
            {
                var team = await _dbContext.Teams.SingleOrDefaultAsync(t => t.Id == id);
                if (team == null) return NotFound();

                return Ok(team);
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpPost(nameof(Add))]
        [Authorize]
        [ValidateModel]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> Add([FromBody] Team team)
        {
            try
            {
                if (await _dbContext.Teams.AnyAsync(t => t.Id == team.Id))
                    return Conflict();

                await _dbContext.Teams.AddAsync(team);
                await _dbContext.SaveChangesAsync();

                var createdAtUri = new Uri($"api/{GetType().Name.Replace("Controller", string.Empty)}/{nameof(Add)}", UriKind.Relative);
                return Created(createdAtUri, team);
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpPut(nameof(Update))]
        [Authorize]
        [ValidateModel]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> Update([FromBody] Team team)
        {
            try
            {
                if (!await _dbContext.Teams.AnyAsync(t => t.Id == team.Id))
                    return NotFound();

                _dbContext.Teams.Update(team);
                await _dbContext.SaveChangesAsync();

                return Ok();
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpDelete(nameof(Remove))]
        [Authorize]
        [ValidateModel]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> Remove([FromQuery] string id)
        {
            if (string.IsNullOrEmpty(id)) return BadRequest();

            try
            {
                var team = _dbContext.Teams.SingleOrDefault(t => t.Id == id);
                if (team == null) return NotFound();

                _dbContext.Teams.Remove(team);
                await _dbContext.SaveChangesAsync();

                return NoContent();
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
    }
}
