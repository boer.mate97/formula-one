﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FormulaOne.TeamManagement.Attributes
{
    public class DatePassed : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            var dateValue = value as DateTime?;
            if (dateValue == null) return false;

            return dateValue.Value < DateTime.Now;
        }
    }
}
