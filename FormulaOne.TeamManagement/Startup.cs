using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using FormulaOne.TeamManagement.Services;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.OAuth;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Server.IISIntegration;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace FormulaOne.TeamManagement
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Add singleton connection handler to keep the in-memory database alive for the full lifetime of the application.
            services.AddSingleton<IDbOptionsProvider, DbConnectionsHandler>(_ => new DbConnectionsHandler(opt =>
            {
                var connection = new SqliteConnection(Configuration.GetConnectionString("SQLite"));
                opt.UseSqlite(connection);
                return connection;
            }));

            services.AddDbContextPool<IFormulaOneDbContext, FormulaOneDbContext>((sp, opt) =>
                sp.GetServices<IDbOptionsProvider>().ToList().ForEach(s => s.Configure(opt)));

            services.AddDbContextPool<IdentityStoreDbContext>((sp, opt) =>
                sp.GetServices<IDbOptionsProvider>().ToList().ForEach(s => s.Configure(opt)));
            services.AddIdentityCore<IdentityUser>(opt =>
            {
                opt.Password.RequireNonAlphanumeric = false;
                opt.Password.RequireUppercase = false;
            })
            .AddEntityFrameworkStores<IdentityStoreDbContext>()
            .AddSignInManager();

            services.AddAuthentication(IdentityConstants.ApplicationScheme)
                .AddCookie(IdentityConstants.ApplicationScheme, opt =>
                {
                    opt.Events.OnRedirectToAccessDenied = (context) =>
                    {
                        context.Response.StatusCode = StatusCodes.Status401Unauthorized;
                        return Task.CompletedTask;
                    };
                    opt.Events.OnRedirectToLogin = (context) =>
                    {
                        context.Response.StatusCode = StatusCodes.Status401Unauthorized;
                        return Task.CompletedTask;
                    };
                });

            services.AddTransient<IDbSeeder, AdminUserSeeder>();
            services.AddTransient<IDbSeeder, TeamsSeeder>();

            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IEnumerable<IDbSeeder> dbSeeders)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            foreach (var seeder in dbSeeders) seeder.Seed().Wait();
        }
    }
}
