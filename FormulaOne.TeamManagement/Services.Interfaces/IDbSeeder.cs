﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FormulaOne.TeamManagement.Services
{
    public interface IDbSeeder
    {
        Task Seed();
    }
}
