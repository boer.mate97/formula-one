﻿using FormulaOne.TeamManagement.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace FormulaOne.TeamManagement.Services
{
    public interface IFormulaOneDbContext
    {
        DatabaseFacade Database { get; }

        DbSet<Team> Teams { get; }

        int SaveChanges();

        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);
    }
}
