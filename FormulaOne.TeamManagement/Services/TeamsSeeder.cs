﻿using FormulaOne.TeamManagement.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FormulaOne.TeamManagement.Services
{
    public class TeamsSeeder : IDbSeeder
    {
        private readonly IFormulaOneDbContext _dbContext;

        public TeamsSeeder(IFormulaOneDbContext dbContext)
        {
            _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
        }

        public async Task Seed()
        {
            try
            {
                for (int i = 0; i < 3; i++)
                {
                    await _dbContext.Teams.AddAsync(new Team()
                    {
                        Id = Guid.NewGuid().ToString("D"),
                        Name = $"Initial team {i}",
                        ChampionshipWinCount = i,
                        EntryFeePaid = i % 2 == 0,
                        FoundedAt = new DateTime(2000, 1, 1).AddYears(i)
                    }); 
                }
                await _dbContext.SaveChangesAsync();
            }
            catch(Exception ex)
            {
                throw new InvalidOperationException($"Seeding {typeof(Team).Name} data has failed.", ex);
            }
        }
    }
}
