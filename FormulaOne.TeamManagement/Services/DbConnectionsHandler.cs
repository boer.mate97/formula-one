﻿using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;

namespace FormulaOne.TeamManagement.Services
{
    // This service handles connections for every DbContext it configures.
    // The lifetime of the connections is equal to the lifetime of the service.
    public class DbConnectionsHandler : IDbOptionsProvider, IDisposable
    {
        private readonly Func<DbContextOptionsBuilder, DbConnection> _connectionOptionsAction;
        private readonly List<DbConnection> _connections;

        public DbConnectionsHandler(Func<DbContextOptionsBuilder, DbConnection> connectionOptionsAction)
        {
            _connectionOptionsAction = connectionOptionsAction ?? throw new ArgumentNullException(nameof(connectionOptionsAction));
            _connections = new List<DbConnection>();
        }

        public void Configure(DbContextOptionsBuilder builder)
        {
            var connection = _connectionOptionsAction(builder);
            connection.Open();
            _connections.Add(connection);
        }

        public void Dispose()
        {
            foreach (var connection in _connections) connection.Close();
        }
    }
}
