﻿using FormulaOne.TeamManagement.Model;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;

namespace FormulaOne.TeamManagement.Services
{
    public class FormulaOneDbContext : DbContext, IFormulaOneDbContext
    {
        public DbSet<Team> Teams { get; set; }

        public FormulaOneDbContext(DbContextOptions<FormulaOneDbContext> options) : base(options)
        {
            Database.Migrate();
        }
    }
}
