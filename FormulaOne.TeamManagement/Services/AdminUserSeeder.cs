﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FormulaOne.TeamManagement.Services
{
    public class AdminUserSeeder : IDbSeeder
    {
        private readonly IConfiguration _configuration;
        private readonly UserManager<IdentityUser> _userManager;

        public AdminUserSeeder(IConfiguration configuration, UserManager<IdentityUser> userManager)
        {
            _configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
            _userManager = userManager ?? throw new ArgumentNullException(nameof(userManager));
        }

        public async Task Seed()
        {
            var userName = _configuration.GetSection("AdminUser")?.GetSection("UserName")?.Value;
            var userPassword = _configuration.GetSection("AdminUser")?.GetSection("Password")?.Value;

            if (userName == null || userPassword == null) throw new InvalidOperationException("Retrieving admin credentials has failed.");

            var user = new IdentityUser()
            {
                Id = Guid.NewGuid().ToString("D"),
                UserName = userName
            };

            var createUserResult = await _userManager.CreateAsync(user);
            if (!createUserResult.Succeeded) throw new InvalidOperationException("Creating the admin user has failed.");

            var addPasswordResult =  await _userManager.AddPasswordAsync(user, userPassword);
            if (!addPasswordResult.Succeeded) throw new InvalidOperationException("Setting the password for the admin user has failed.");
        }
    }
}
