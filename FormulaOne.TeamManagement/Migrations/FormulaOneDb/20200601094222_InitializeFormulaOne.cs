﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FormulaOne.TeamManagement.Migrations.FormulaOneDb
{
    public partial class InitializeFormulaOne : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Teams",
                columns: table => new
                {
                    Id = table.Column<string>(maxLength: 36, nullable: false),
                    Name = table.Column<string>(maxLength: 30, nullable: false),
                    FoundedAt = table.Column<DateTime>(nullable: false),
                    ChampionshipWinCount = table.Column<int>(nullable: true),
                    EntryFeePaid = table.Column<bool>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Teams", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Teams");
        }
    }
}
