﻿// <auto-generated />
using System;
using FormulaOne.TeamManagement.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace FormulaOne.TeamManagement.Migrations.FormulaOneDb
{
    [DbContext(typeof(FormulaOneDbContext))]
    partial class FormulaOneDbContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "3.1.4");

            modelBuilder.Entity("FormulaOne.TeamManagement.Model.Team", b =>
                {
                    b.Property<string>("Id")
                        .HasColumnType("TEXT")
                        .HasMaxLength(36);

                    b.Property<int?>("ChampionshipWinCount")
                        .HasColumnType("INTEGER");

                    b.Property<bool?>("EntryFeePaid")
                        .HasColumnType("INTEGER");

                    b.Property<DateTime?>("FoundedAt")
                        .IsRequired()
                        .HasColumnType("TEXT");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("TEXT")
                        .HasMaxLength(30);

                    b.HasKey("Id");

                    b.ToTable("Teams");
                });
#pragma warning restore 612, 618
        }
    }
}
